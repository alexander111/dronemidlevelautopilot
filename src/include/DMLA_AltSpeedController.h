/*
 * AltSpeedController.h
 *
 *  Created on: 11/06/2012
 *      Author: Ignacio Mellado-Bataller
 *  Modified on: 20/08/2014
 *      by 1: Jesus Pestana Puerta
 *      by 2: Miguel Gomez Gonzalez
 */

#ifndef ALTSPEEDCONTROLLER_H_
#define ALTSPEEDCONTROLLER_H_

#include <string>
#include <math.h>

#include "control/Filter.h"
#include <iostream>
#include "xmlfilereader.h"
#include "Timer.h"

//#define ALTSPEEDCONTROLLER_USE_DMC_CONTROL_INSTEAD_OF_PID
#ifndef ALTSPEEDCONTROLLER_USE_DMC_CONTROL_INSTEAD_OF_PID
#include "control/PID.h"
#else
#include "control/DMC.h"
#endif


namespace CVG {
namespace MAV {

class AltSpeedController {
private:
#ifndef ALTSPEEDCONTROLLER_USE_DMC_CONTROL_INSTEAD_OF_PID
    CVG_BlockDiagram::PID controller_module;
#else
    CVG_BlockDiagram::DMC controller_module;
#endif
    double thrust_1p1;
    double lastAlt;
    bool started;
    Timer dTimer;
    bool enabled;

    int debugCount;
    Timer debugTimer;
    int repeatCount;
    double lastSpeed, lastSpeedF;
    CVG_BlockDiagram::Filter outputFilter;

public:

    void init(std::string configFile);
    bool readConfigs(std::string configFile);

public:
    AltSpeedController(int idDrone, const std::string &stackPath_in);

    void updateAltitude(double altitude, double maxAltSpeedCommand); // Nacho's Function
    void updateAltitude(double altitude, double altitude_speed, double maxAltSpeedCommand);
    void setReference(double ref);
    double getOutput();

    void debugRef(const std::string &str);
    void debugInfo(const std::string &str);

    void enable(bool e);

private:
    // Configuration parameters
    double FEEDFORWARD;
    double GAIN_P, GAIN_I, GAIN_D;

    double ALT_SPEED_MEASURE_SATURATION_FACTOR;

    double COMMAND_UPPER_LIMIT;
    double COMMAND_LOWER_LIMIT;
    int    SUBSAMPLING_DIVIDER;
};

}
}

#endif /* ALTSPEEDCONTROLLER_H_ */
