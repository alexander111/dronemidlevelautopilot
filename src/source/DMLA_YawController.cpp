/*
 * Linkquad_YawController.cpp
 *
 *  Created on: 24/10/2012
 *      Author: Ignacio Mellado-Bataller
 */

#include "DMLA_YawController.h"

namespace CVG
{
namespace MAV
{

YawController::YawController(int idDrone, const std::string &stackPath_in)
{
      std::cout << "YawController(...), idDrone:" << idDrone << " stackPath:" << stackPath_in << std::endl;
}

bool YawController::readConfigs(std::string configFile)
{
  try {
      XMLFileReader my_xml_reader(configFile);

      GAIN_P = my_xml_reader.readDoubleValue("midlevel_autopilot_config:yaw_controller:GAIN_P");
      GAIN_I = my_xml_reader.readDoubleValue("midlevel_autopilot_config:yaw_controller:GAIN_I");
      GAIN_D = my_xml_reader.readDoubleValue("midlevel_autopilot_config:yaw_controller:GAIN_D");

  } catch ( cvg_XMLFileReader_exception &e) {
      throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
  }
}

void YawController::init(std::string configFile)
{
    readConfigs(configFile);

    yawPid.setGains(GAIN_P, GAIN_I, GAIN_D);
    enabled = false;
    reference = feedback = 0.0;

    std::cout << "YawController(...), exit "<< std::endl;
}

void YawController::setReference(double yawRef_deg)
{
    reference = yawRef_deg;
    calcError();
}

void YawController::setFeedback(double yawMeasure_deg)
{
    feedback = yawMeasure_deg;
    calcError();
}

void YawController::calcError()
{
    double err = fmod(reference - feedback, 360.0);
    if (err < -180) err += 360;
    else if (err > 180) err -= 360;
    yawPid.setReference( 0.0 );
    yawPid.setFeedback( -err );
}

double YawController::getOutput()
{
    return yawPid.getOutput();
}

void YawController::enable(bool e)
{
    if (enabled != e)
    {
      yawPid.reset();
    }

    enabled = e;
}

}
}

